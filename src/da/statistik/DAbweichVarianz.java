package da.statistik;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class DAbweichVarianz {
public void calc() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("/home/daniel/Dokumente/uni/Mathe3_2/Statistik/zahlen"));
        String s;
        double mittel = 0;

        while ((s = br.readLine()) != null) {
                mittel += Integer.parseInt(s);
        }
        br.close();
        mittel /= 60;
        System.out.println(mittel);

        br = new BufferedReader(new FileReader("/home/daniel/Dokumente/uni/Mathe3_2/Statistik/zahlen"));
        double sum = 0;
        while ((s = br.readLine()) != null) {
                sum += Math.pow(Integer.parseInt(s) - mittel, 2);
        }
        System.out.println(Math.sqrt(sum/(60-1))); //Standardabweichung
        System.out.println(sum/(60-1)); // empirische varianz
}
}
