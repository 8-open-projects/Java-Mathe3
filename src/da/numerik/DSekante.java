package da.numerik;

import da.interfaces.Funktion;
import da.interfaces.Verfahren;

public class DSekante implements Verfahren {
final private int maxVersuche = 100;
private int n = 0;
@Override
public void calc(Funktion f, double a, double b, double e) {
        n = 0;
        sek(f, a, b, e, maxVersuche);
        System.out.println("Berechnungen: "+n);
}
private double sek(Funktion f, double x0, double x1, double e, int maxVersuche) {
        System.out.println("x0: "+x0+"\nx1: "+x1);
        while (Math.abs(f.func(x1)) > e && n < maxVersuche) {
                double x2 = x0-((x1-x0)/(f.func(x1)-f.func(x0)))*f.func(x0);
                System.out.println("x"+(n+2)+": "+x2);
                x0 = x1;
                x1 = x2;
                n++;
        }
        if (Math.abs(f.func(x1)) <= e) {
                return x1;
        } else {
                return 0.0;
        }
}
}
