package da.numerik;

import da.interfaces.Funktion;
import da.interfaces.Verfahren;

public class DBisektion implements Verfahren {
int i = 0;
@Override
public void calc(Funktion f, double a, double b, double e) {
        System.out.println(
                bis(f, a, b, e)
                );
        System.out.println("Benötigte Iterationen: "+i);
}
private double bis(Funktion f, double a, double b, double e) {
        if (f.func(a)*f.func(b) > 0) return 0.0;

        double x = (a+b)/2.0;
        System.out.println(x);
        i++;


        if (Math.abs(b-a)<e) {
                return x;
        } else {
                if (f.func(a)*f.func(x) < 0) {
                        return bis(f, a, x, e);
                } else {
                        return bis(f, x, b, e);
                }
        }
}
}
