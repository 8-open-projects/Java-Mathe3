package da.calculator;

import da.interfaces.Funktion;
import da.numerik.DBisektion;
import da.numerik.DSekante;

public class DCalculator {
public static void main(String[] args) {
        double a = 1.2;
        double b = 2.2;
        double e = 1.0E-5;
        DBisektion rechner1 = new DBisektion();
        rechner1.calc(new Funktion() {

                        @Override
                        public double func(double x) {
                                return 6.0 * Math.sin(5.0 * x - 5.0);
                        }
                }, a, b, e);
        DSekante rechner2 = new DSekante();
        rechner2.calc(new Funktion() {

                        @Override
                        public double func(double x) {
                                return 6.0 * Math.sin(5.0 * x - 5.0);
                        }
                }, a, b, e);
}
}
