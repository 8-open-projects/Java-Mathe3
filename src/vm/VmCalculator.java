package vm.calculator;

import vm.numerik.VmBisektion;
import da.interfaces.Funktion;
import da.numerik.DBisektion;

public class VmCalculator {
public static void main(String[] args) {
								double a = 1.2;
								double b = 2.2;
								double base = 10.0;
								double exp = 3.0;
								double e = base/(Math.pow(10,exp));
								/*
								 * bisektion double a = 0.0; double b = 2*Math.PI; double e = 0.00005;
								 */
								DBisektion rechner = new DBisektion();
								rechner.calc(new Funktion() {

																								@Override
																								public double func(double x) {
																																return Math.pow(x, 3) - 31 * Math.pow(x, 2) + x + 1; // sekante
																								        // return x - Math.sin(x) - 0.4 * Math.PI; // bisektion
																								}
																}, a, b, e);

								Funktion f1 = new Funktion() {
																@Override
																public double func(double x) {
																								return Math.pow(x, 3) - 31 * Math.pow(x, 2) + x + 1;
																}
								};

								Funktion f2 = new Funktion() {
																@Override
																public double func(double x) {
																								return 6.0 * Math.pow(x, 3) - 12.0 * Math.pow(x, 2) + 2 * x
																															+ 20.0 / 9.0;
																}
								};

								Funktion f3 = new Funktion() {
																@Override
																public double func(double x) {
																								return 9.0 * Math.sin(4.0 * x + 6.0);
																}
								};

								Funktion f4 = new Funktion() {
																@Override
																public double func(double x) {
																								return 6.0 * Math.sin(5.0 * x - 5.0);
																}
								};

								double bisektion = VmBisektion.getBis(f4, a, b, e);

								System.out.println("VM: " + bisektion + " itertaionen: " + VmBisektion.it);
								System.out.println("Ben�tigte Iterationen: " + VmBisektion.iteratonen(base,exp));
}
}
