package vm.numerik;

import da.interfaces.Funktion;

public class VmBisektion {
public static int it = 0;

public static int iteratonen(double base, double exp) {
								return (int) (exp * (Math.log10(base) / Math.log10(2))) + 1;
}

public static double getBis(Funktion f, double x1, double x2, double exit) {
								try {
																System.out.println(it++);

																if ((f.func(x1) * f.func(x2)) > 0)
																								return 0.0;

																// x1 = Math.round(x1 * 10000.0) / 10000.0;
																// x2 = Math.round(x2 * 10000.0) / 10000.0;

																// double newX = Math.round(((x1 + x2) / 2.0) * 10000.0) / 10000.0;
																double newX = (x1 + x2) / 2.0;
																System.out.println("X-New: " + newX);

																// abbruch bedingung fals absolutwert kleiner exit ist
																if (Math.abs(x2 - x1) < exit)
																								return newX;
																else { // sonst rechne neuen wert
																								if (f.func(x1) * f.func(newX) < 0) {
																																System.out.println("funk= " + f.func(x1) * f.func(newX));
																																System.out.println("X1: " + x1 + " X-New: " + newX);
																																System.out.println("-----------------------------");
																																return getBis(f, x1, newX, exit);
																								} else {
																																System.out.println("funk= " + f.func(x1) * f.func(newX));
																																System.out.println("X2: " + x2 + " X-New: " + newX);
																																System.out.println("-----------------------------");
																																return getBis(f, x2, newX, exit);
																								}
																}
								} catch (Exception e) {
																System.out.println("VM bisektion failed!!!");
								}
								return -1;
}
}
